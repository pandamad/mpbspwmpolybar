# Icons themes fonts
sudo cp -r /tmp/mpbspwmpolybar/fonts/* /usr/share/fonts
cp -rf /tmp/mpbspwmpolybar/utils/config/* ~/.config
mv /tmp/mpbspwmpolybar/utils/Xresources.d /tmp/mpbspwmpolybar/utils/.Xresources.d
cp -rf /tmp/mpbspwmpolybar/utils/.Xresources.d $HOME
cp -rf /tmp/mpbspwmpolybar/utils/.Xresources $HOME
cp -rf /tmp/mpbspwmpolybar/utils/.gtkrc-2.0 $HOME
cp -rf /tmp/mpbspwmpolybar/utils/.xsettingsd $HOME
sudo cp -rf /tmp/mpbspwmpolybar/utils/apps_as_root.sh /usr/local/bin
sudo cp -rf /tmp/mpbspwmpolybar/utils/nmd /usr/local/bin
sudo cp -rf /tmp/mpbspwmpolybar/utils/askpass_rofi.sh /usr/local/bin


# Install base apps
sudo apt install bspwm sxhkd rofi compton polybar dunst -y &&
sudo apt install gcc make libx11-dev libxtst-dev pkg-config -y &&
sudo apt install neofetch htop feh lxappearance geany dmenu nm-tray xfconf xsettingsd xfce4-power-manager zenity git arc-theme -y &&
sudo apt install thunar-archive-plugin catfish baobab meld -y &&
sudo apt install mate-polkit i3lock scrot -y &&
sudo apt install rxvt-unicode alacritty ranger cmus xterm pcmanfm xfce4-terminal code-oss gcolor3 -y

# Copy themes
cd /tmp && git clone https://github.com/archcraft-os/archcraft-themes && chmod 755 archcraft-themes/* -R
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-adapta/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-arc/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-blade/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-cyberpunk/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-dracula/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-groot/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-gruvbox/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-hack/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-juno/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-kripton/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-manhattan/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-nordic/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-rick/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-spark/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-sweet/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-wave/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-white/files/* /usr/share/themes
sudo cp -rf /tmp/archcraft-themes/archcraft-gtk-theme-windows/files/* /usr/share/themes
git clone https://github.com/vinceliuice/matcha && chmod 755 matcha/* -R && cd matcha && ./install.sh & cd ..

# Copy icons
cd /tmp && git clone https://github.com/archcraft-os/archcraft-icons && chmod 755 archcraft-icons/* -R
sudo cp -rf /tmp/archcraft-icons/archcraft-icons-arc/files/* /usr/share/icons
sudo cp -rf /tmp/archcraft-icons/archcraft-icons-breeze/files/* /usr/share/icons
sudo cp -rf /tmp/archcraft-icons/archcraft-icons-hack/files/* /usr/share/icons
sudo cp -rf /tmp/archcraft-icons/archcraft-icons-luna/files/* /usr/share/icons
sudo cp -rf /tmp/archcraft-icons/archcraft-icons-luv/files/* /usr/share/icons
sudo cp -rf /tmp/archcraft-icons/archcraft-icons-nordic/files/* /usr/share/icons
sudo cp -rf /tmp/archcraft-icons/archcraft-icons-numix/files/* /usr/share/icons
sudo cp -rf /tmp/archcraft-icons/archcraft-icons-papirus/files/* /usr/share/icons
sudo cp -rf /tmp/archcraft-icons/archcraft-icons-qogir/files/* /usr/share/icons
sudo cp -rf /tmp/archcraft-icons/archcraft-icons-white/files/* /usr/share/icons
sudo cp -rf /tmp/archcraft-icons/archcraft-icons-win11/files/* /usr/share/icons
sudo cp -rf /tmp/archcraft-icons/archcraft-icons-zafiro/files/* /usr/share/icons

# Copy utils
cp -rf /tmp/mpbspwmpolybar/utils/.fehbg $HOME
cp -rf /tmp/mpbspwmpolybar/utils/Thunar/ ~/.config
cp -rf /tmp/mpbspwmpolybar/utils/sxhkd/ ~/.config
mkdir -p ~/.config/neofetch
cp -rf /tmp/mpbspwmpolybar/utils/config.conf ~/.config/neofetch
cp -rf /tmp/mpbspwmpolybar/utils/powermenu ~/.config/bspwm/rofi/bin
cp -rf /tmp/mpbspwmpolybar/utils/powermenu.rasi ~/.config/bspwm/rofi/themes
sudo cp -rf /tmp/mpbspwmpolybar/utils/nordico.png /usr/share/images/desktop-base/
sudo cp -rf /tmp/mpbspwmpolybar/utils/lightdm-gtk-greeter.conf /etc/lightdm/