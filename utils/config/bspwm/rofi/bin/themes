#!/usr/bin/env bash

## Copyright (C) 2020-2021 Aditya Shakya <adi1090x@gmail.com>
## Everyone is permitted to copy and distribute copies of this file under GNU-GPL3

DIR="$HOME/.config/bspwm"
BSP="$HOME/.config/bspwm/themes"
rofi_command="rofi -theme $DIR/rofi/themes/themes.rasi"

# Themes
theme_1=" Beach"
theme_2=" Blade"
theme_3=" Bouquet"
theme_4=" Cyberpunk"
theme_5=" Dracula"
theme_6=" Flowers"
theme_7=" Forest"
theme_8=" Groot"
theme_9=" Gruvbox"
theme_10=" Hack"
theme_11=" Keyboards"
theme_12=" Light"
theme_13=" Manhattan"
theme_14=" Nord"
theme_15=" Nordic"
theme_16=" Pastel"
theme_17=" Rick"
theme_18=" Spark"
theme_19=" Tealize"
theme_20=" Wave"

# Variable passed to rofi
options="$theme_1\n$theme_2\n$theme_3\n$theme_4\n$theme_5\
\n$theme_6\n$theme_7\n$theme_8\n$theme_9\n$theme_10\
\n$theme_11\n$theme_12\n$theme_13\n$theme_14\n$theme_15\
\n$theme_16\n$theme_17\n$theme_18\n$theme_19\n$theme_20"

chosen="$(echo "$options" | $rofi_command -p "Available themes: (20)" -dmenu -selected-row 0)"
case $chosen in
    $theme_1)
        bash ${BSP}/set-theme  'beach.jpg' 'beach' 'Arc-Lighter' 'Arc-Circle' 'Fluent' && feh --bg-scale ~/.config/bspwm/themes/beach.jpg
        ;;
    $theme_2)
        bash ${BSP}/set-theme  'blade.jpg' 'blade' 'Blade' 'Nordic-Folders' 'LyraS' && feh --bg-scale ~/.config/bspwm/themes/blade.jpg
        ;;
    $theme_3)
        bash ${BSP}/set-theme  'bouquet.jpg' 'bouquet' 'Matcha-dark-aliz' 'Zafiro-Red' 'Future-dark' && feh --bg-scale ~/.config/bspwm/themes/bouquet.jpg
        ;;
    $theme_4)
        bash ${BSP}/set-theme  'cyberpunk.jpg' 'cyberpunk' 'Cyberpunk' 'Archcraft-Dark' 'LyraB' && feh --bg-scale ~/.config/bspwm/themes/cyberpunk.jpg
        ;;
    $theme_5)
        bash ${BSP}/set-theme  'dracula.png' 'dracula' 'Dracula' 'Nordic-Folders' 'Sweet' && feh --bg-scale ~/.config/bspwm/themes/dracula.jpg
        ;;
    $theme_6)
        bash ${BSP}/set-theme  'flowers.jpg' 'flowers' 'Kripton' 'Zafiro' 'Fluent-dark'  && feh --bg-scale ~/.config/bspwm/themes/flowers.jpg
        ;;
    $theme_7)
        bash ${BSP}/set-theme  'forest.jpg' 'forest' 'Adapta-Nokto' 'Luv-Folders-Dark' 'Vimix'  && feh --bg-scale ~/.config/bspwm/themes/forest.jpg
        ;;
    $theme_8)
        bash ${BSP}/set-theme  'groot.jpg' 'groot' 'Groot' 'Luna-Dark' 'Pear' && feh --bg-scale ~/.config/bspwm/themes/groot.jpg
        ;;
    $theme_9)
        bash ${BSP}/set-theme  'gruvbox.jpg' 'gruvbox' 'Gruvbox' 'Luna-Dark' 'Future-dark' && feh --bg-scale ~/.config/bspwm/themes/gruvbox.jpg
        ;;
    $theme_10)
        bash ${BSP}/set-theme  'hack.jpg' 'hack' 'Hack' 'Hack' 'LyraB' && feh --bg-scale ~/.config/bspwm/themes/hack.jpg
        ;;
    $theme_11)
        bash ${BSP}/set-theme  'keyboards.jpg' 'keyboards' 'Sweet-Dark' 'Zafiro-Purple' 'Sweet' && feh --bg-scale ~/.config/bspwm/themes/keyboards.jpg
        ;;
    $theme_12)
        bash ${BSP}/set-theme  'light.jpg' 'light' 'White' 'Arc-Circle' 'Fluent-dark' && feh --bg-scale ~/.config/bspwm/themes/light.jpg
        ;;
    $theme_13)
        bash ${BSP}/set-theme  'manhattan.jpg' 'manhattan' 'Manhattan' 'Luv-Folders-Dark' 'Vimix-dark' && feh --bg-scale ~/.config/bspwm/themes/manhattan.jpg
        ;;
    $theme_14)
        bash ${BSP}/set-theme  'nord.jpg' 'nord' 'Arc-Dark' 'Arc-Circle' 'Qogirr' && feh --bg-scale ~/.config/bspwm/themes/nord.jpg
        ;;
    $theme_15)
        bash ${BSP}/set-theme  'nordic.jpg' 'nord' 'Nordic' 'Nordic-Folders' 'Qogirr-dark' && feh --bg-scale ~/.config/bspwm/themes/nordic.jpg
        ;;
    $theme_16)
        bash ${BSP}/set-theme  'pastel.png' 'pastel' 'White' 'Qogir' 'Qogirr-dark' && feh --bg-scale ~/.config/bspwm/themes/pastel.jpg
        ;;
    $theme_17)
        bash ${BSP}/set-theme  'rick.jpg' 'rick' 'Rick' 'Zafiro' 'Vimix-dark' && feh --bg-scale ~/.config/bspwm/themes/rick.jpg
        ;;
    $theme_18)
        bash ${BSP}/set-theme  'spark.jpg' 'spark' 'Spark' 'Luv-Folders' 'Vimix' && feh --bg-scale ~/.config/bspwm/themes/spark.jpg
        ;;
    $theme_19)
        bash ${BSP}/set-theme  'tealize.png' 'tealize' 'Juno-palenight' 'Luv-Folders-Dark' 'Vimix' && feh --bg-scale ~/.config/bspwm/themes/tealize.jpg
        ;;
    $theme_20)
        bash ${BSP}/set-theme  'wave.jpg' 'wave' 'Wave' 'Luv-Folders-Dark' 'Vimix' && feh --bg-scale ~/.config/bspwm/themes/wave.jpg
        ;;
esac