# MPbspwmPolybay

Custom script to install BSPWM and Polybar with some themes and utils. 

- [BSPWM](https://github.com/baskerville/bspwm)
- [Polybar](https://github.com/polybar/polybar)

## Getting Started

This is what will be installed on your system:

> bspwm, sxhkd, rofi, compton, polybar, dunst, ksuperkey 3, neofetch, htop, feh, lxappearance, geany, dmenu, nm-tray, xfconf, xsettingsd, xfce4-power-manager, zenity, git.

## Prerequisites

Requirements for the installation is git and Xorg or other X software.

## Installing

To install you need to put this command:
```bash
sudo apt install git -y && cd /tmp && git clone https://gitlab.com/pandamad/mpbspwmpolybar  && chmod 755 mpbspwmpolybar/* -R && cd mpbspwmpolybar/ && ./install.sh
```

# Shortcuts in BSPWM
*BSPWM* is to use the most from keyboard. Here are some shortcuts to get you between windows and some utilities.

To edit, remove or add new shortcuts you can edit in: `~/.config/sxhkd/sxhkdrc`.

<br>(Caption: <kbd> W</kbd> = Windows key)

## Menus
| KEY | Action |
| --- | --- |
| <kbd>W</kbd> or <kbd>alt</kbd> or <kbd>F1</kbd> | Aplications menu |
| <kbd>W</kbd> + <kbd>w</kbd> | Opened programs (Windows key and "w") |
| <kbd>W</kbd> + <kbd>x</kbd> | Shutdown menu options |
| <kbd>W</kbd> + <kbd>d</kbd> | dmenu (alternative to Rofi) |
| <kbd>W</kbd> + <kbd>N</kbd> | Connections menu |
| <kbd>Ctrl</kbd> + <kbd>alt</kbd> + <kbd>t</kbd> | Theme selector |

## Commands
| KEY | Action |
| --- | --- |
| <kbd>W</kbd> + <kbd>esc</kbd> | Reload shortcuts (from sxhkd) |
| <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>r</kbd> | Reload BSPWM |
| <kbd>W</kbd> + <kbd>l</kbd> | Screen lock |

## Aplications
| KEY | Action |
| --- | --- |
| <kbd>W</kbd> + <kbd>Enter</kbd> | Terminal |
| <kbd>W</kbd> + <kbd>e</kbd> | File manager (thunar or nautilus) |
| <kbd>W</kbd> + <kbd>Shift</kbd> + <kbd>w</kbd> | Firefox |
| <kbd>W</kbd> + <kbd>Shift</kbd> + <kbd>e</kbd> | Geany |
| <kbd>W</kbd> + <kbd>c</kbd> | Close focused window |

## Working areas
| Key | Action |
| --- | --- |
| <kbd>W</kbd> + <kbd>1-8</kbd> | Change workspace |
| <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>←/→</kbd> | Change workspace to next or previous |

## Manage windows
| Key | Action |
| --- | --- |
| <kbd>W</kbd> + <kbd>Shift</kbd> + <kbd>1-8</kbd> | Send app to specific workspace |
| <kbd>W</kbd> + <kbd>Space bar</kbd>| Change between tiling / floating |
| <kbd>W</kbd> + <kbd>F</kbd> | Change to Full screen |
| <kbd>W</kbd> + <kbd>H/V/Q</kbd> | Repair app split in Horizontal, Vertical or Cancel |
| <kbd>W</kbd> + <kbd>TAB</kbd> | Back to last opened app |
| <kbd>W</kbd> + <kbd>Ctrl</kbd> + <kbd>←/→/↑ /↓ </kbd> | Expand focused window |
| <kbd>W</kbd> + <kbd>Alt</kbd> + <kbd>←/→/↑ /↓</kbd> | Shrink focused window |

<hr>

## Built With

  - [Contributor Covenant](https://www.contributor-covenant.org/) - Used
    for the Code of Conduct
  - [Creative Commons](https://creativecommons.org/) - Used to choose
    the license

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code
of conduct, and the process for submitting pull requests to us.

## Author

  - **ViODREAM** 
  - *Based on work of* 
    [Thespation](https://plus.diolinux.com.br/t/script-instale-as-personalizacoes-e-temas-archcraft-em-diversas-distros/40325)

You are free to contact:

    **ViODREAM** [on Gitlab](https://gitlab.com/viodream)

## License

This project is licensed under the [CC0 1.0 Universal](LICENSE.md)
Creative Commons License - see the [LICENSE.md](LICENSE.md) file for
details

## Acknowledgments

  - Hat tip to anyone whose code is used
  - Inspiration
  - etc
