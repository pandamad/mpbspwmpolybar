#!/usr/bin/env bash
# Desenvolvido pelo William Santos
# contato: thespation@gmail.com ou https://github.com/thespation

# Cores (tabela de cores: https://gist.github.com/avelino/3188137)
VERM="\033[1;31m"	#Output red
VERD="\033[0;32m"	#Output green
CIAN="\033[0;36m"	#Output cyan
NORM="\033[0m"		#Back default color

# Ends, in case of runtime error
set -e

# Alias
DATA=`date +%F.%T`		PASTA="$HOME/.config/"	
PTMP='/tmp/dpux_bspwm/home/xubuntu/'
COP="cp -rf"			SCOP="sudo cp -rf"

# Copy custom files to Xubuntu
COPI () {
	#echo -e "\n${CIAN}[ ] Start copy process ${NORM}
		${COP} ${PTMP}.fehbg $HOME									# File responsible for the wallpaper
		${COP} ${PTMP}powermenu ${PASTA}bspwm/rofi/bin				#Files responsible for shutdown menu
		${SCOP} ${PTMP}nordico.png /usr/share/xfce4/backdrops/		#Nordic wallpaper for Xubuntu
		PNEOF && mkdir -p ${PASTA}/neofetch							#Files for neofetch custom display
		${COP} ${PTMP}config.conf ${PASTA}/neofetch					#Files for neofetch custom display
		PTHUN && ${COP} ${PTMP}Thunar/ ${PASTA}						#Files for custom actions in Thunar
		PSXHK && ${COP} ${PTMP}sxhkd/ ${PASTA}						# Responsible for shortcut keys
		PLIGH && ${SCOP} ${PTMP}lightdm-gtk-greeter.conf /etc/lightdm/		#LightDM Customization
	echo -e "${VERD}[*] Copied customization files" ${NORM}
}

# Check if the neofetch folder exists, if you have it, make a backup before playing the file
PNEOF () {
	if [[ -d "${PASTA}neofetch" ]]; then
		echo -e "\n${CIAN}[ ] Backing up the folder ${PASTA}neofetch" ${NORM}
		mv ${PASTA}neofetch ${PASTA}neofetch.bkp.${DATA}	
		echo -e "${VERD}[*] Backup performed successfully" ${NORM}
	fi
}

# Check if the Thunar folder exists, if you have it, make a backup before playing the folder
PTHUN () {
	if [[ -d "${PASTA}Thunar" ]]; then
		echo -e "\n${CIAN}[ ] Backing up the folder ${PASTA}Thunar" ${NORM}
		mv ${PASTA}Thunar ${PASTA}Thunar.bkp.${DATA}
		echo -e "${VERD}[*] Backup performed successfully" ${NORM}
	fi
}

# Check if the sxhkdrc file exists, if it has a backup before playing the file
PSXHK () {
	if [[ -d ${PASTA}sxhkd ]]; then
		echo -e "\n${CIAN}[ ] Backing up the file sxhkd" ${NORM}
		mv ${PASTA}sxhkd ${PASTA}sxhkd.bkp.${DATA}	
		echo -e "${VERD}[*] Backup performed successfully" ${NORM}
	fi
}

# Back up before playing the lightdm-gtk-greeter.conf file
PLIGH () {
	if [[ -f /etc/lightdm/lightdm-gtk-greeter.conf ]]; then
		echo -e "\n${CIAN}[ ] Backing up the file lightdm-gtk-greeter.conf" ${NORM}
		sudo mv /etc/lightdm/lightdm-gtk-greeter.conf /etc/lightdm/lightdm-gtk-greeter.conf.bkp.${DATA}	
		echo -e "${VERD}[*] Backup performed successfully" ${NORM}
	fi
}

# Start script
COPI
