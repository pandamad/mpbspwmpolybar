#!/usr/bin/env bash
# Desenvolvido pelo William Santos
# contato: thespation@gmail.com ou https://github.com/thespation

# Cores (tabela de cores: https://gist.github.com/avelino/3188137)
VERM="\033[1;31m"	#Make the output red
VERD="\033[0;32m"	#Make the output green
CIAN="\033[0;36m"	#Make the output cyan
NORM="\033[0m"		#Back to default color
## Alias
CC="sudo cp -rf"		#file copy command
GAT="/tmp/archcraft-icons/"	#Temporary icon folder
USRT="/usr/share/icons/"	#System icons folder
FP="/files/*"			#Alias for end of icon folders name

#Funções
	# BAT - Test/Download if the icon folder exists
	# INSTEMA - copy icons to system folder
BAT () {
	#Test if you have already downloaded the icons
	if [[ -d "${GAT}" ]]; then #Checks if the icons temporary folder exists
		INSTEMA
	elif [[ ! -d "${GAT}" ]]; then #If not, download the icons
		echo -e "\n${CIAN}[ ] Downloading icons for" ${GAT} ${NORM}
		cd /tmp && git clone https://github.com/archcraft-os/archcraft-icons &&
		chmod 755 archcraft-themes/* -R #Grant permission to the downloaded folder
		echo -e "${VERD}[*] icons in the folder" ${GAT} ${NORM}
		INSTEMA
	else
		echo -e "\n${VERM}[!] Unable to copy icons\n" ${NORM} #Notifies in case of failure
	fi	
}	
	
INSTEMA () {
		echo -e "\n${CIAN}[ ] Copiando os ícones para" $USRT ${NORM} # Copying Icons to the Icons Folder "/usr/share/themes"
		$CC $GAT'archcraft-icons-arc'$FP $GAT'archcraft-icons-breeze'$FP $GAT'archcraft-icons-hack'$FP \
		$GAT'archcraft-icons-luna'$FP $GAT'archcraft-icons-luv'$FP $GAT'archcraft-icons-nordic'$FP \
		$GAT'archcraft-icons-numix'$FP $GAT'archcraft-icons-papirus'$FP $GAT'archcraft-icons-qogir'$FP \
		$GAT'archcraft-icons-white'$FP $GAT'archcraft-icons-win11'$FP $GAT'archcraft-icons-zafiro'$FP $USRT
				
	if [[ -d "$USRT/Luv-Folders-Dark" || "$USRT/Nordic-Folders" ]]; then #Checks if the folders are in the right place
		ls $USRT #Exibe a pasta de ícones
		echo -e "${VERD}[*] Icons copied to folder" $USRT ${NORM} #Notify if success
	else
		echo -e "\n${VERM}[!] Unable to copy icons\n" ${NORM} #Notifies in case of failure
	fi
}
# Start icon verification and installation process
BAT
