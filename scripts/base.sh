#!/usr/bin/env bash
# Desenvolvido pelo William Santos
# contato: thespation@gmail.com ou https://github.com/thespation

# Cores (tabela de cores: https://gist.github.com/avelino/3188137)
VERM="\033[1;31m"	#Output red
VERD="\033[0;32m"	#Output green
CIAN="\033[0;36m"	#Output cyan
NORM="\033[0m"		#Back default color
# Alias de instalação
SUDD='sudo apt' 	#Base Debian
SUDF='sudo dnf' 	#Fedora
INXI=`inxi -S` 		#Needed to identify the DE (xfce or gnome)
GIT='https://raw.githubusercontent.com/thespation/dpux_bspwm/main/scripts/' #scripts folder
CURL='curl -s'		#Command for executing the script, without downloading
# packages for installation
PACP="bspwm sxhkd rofi picom polybar dunst" #Base BSPWM
PACC="neofetch htop feh geany dmenu nm-tray xfconf xsettingsd xfce4-power-manager" #All systems
PACDG="mate-polkit i3lock xsetroot" #For GNOME
PACDX="thunar-archive-plugin catfish baobab meld" #Only for Xfce. For Xubuntu "language-selector-gnome"

#Responsible for updating the system, installing the BSPWM base and complementary apps
ATUAS () { 
	if [[ ${INXI} = *Debian* || ${INXI} = *Ubuntu* || ${INXI} = *Pop* ]]; then #Test if base is Debian
		echo -e "\n${CIAN}[ ] Update system" ${NORM}
			${SUDD} update && ${SUDD} upgrade -y && ${SUDD} dist-upgrade -y &&
			${SUDD} autoclean && ${SUDD} autoremove -y &&
		echo -e "${VERD}[*] System updated" ${NORM} && sleep 3s
		echo -e "\n${CIAN}[ ] Install base BSPWM" ${NORM}
			${SUDD} install ${PACP} -y &&
		echo -e "${VERD}[*] Base BSPWM installed successfull: \"${PACP}\"" ${NORM}
		echo -e "\n${CIAN}[ ] Install complementary APPs" ${NORM}
			${SUDD} install ${PACC} -y &&
		echo -e "${VERD}[*] Apps installed: \"${PACC}\" " ${NORM}
		KSUD
	elif [[ ${INXI} = *Fedora* ]]; then
		echo -e "\n${CIAN}[ ] Update system" ${NORM}
			${SUDF} update -y && ${SEGU} &&
		echo -e "${VERD}[*] System updated" ${NORM} && sleep 3s
		
		echo -e "\n${CIAN}[ ] Install base BSPWM" ${NORM}
			${SUDF} -y ${PACP} &&
		echo -e "${VERD}[*] Base BSPWM instalada com sucesso" ${NORM}
		KSUF
	else
		echo -e "${VERM}[!] Unsupported system, DE, or version\n" ${NORM}
	fi
}

#Responsible for enabling start menu on super key, Debian base
KSUD () { 
	echo -e "\n${CIAN}[ ] Enable Super key to open menu" ${NORM}
		${SUDD} install gcc make libx11-dev libxtst-dev pkg-config -y &&
		cd /tmp && git clone https://github.com/hanschen/ksuperkey.git &&
		cd ksuperkey
		make && sudo make install &&
	echo -e "${VERD}[*] Super key enabled successfully" ${NORM}
}

#Responsible for enabling start menu on super key, Fedora
KSUF () { 
	echo -e "\n${CIAN}[ ] Enable Super key to open menu" ${NORM}
		${SUDF} install -y snapd &&
		sudo ln -s /var/lib/snapd/snap /snap && sudo snap wait system seed.loaded
		sudo systemctl enable --now snapd.socket && sudo systemctl start --now snapd.socket
		sudo systemctl restart --now snapd.socket && sudo snap install ksuperkey		
	echo -e "${VERD}[*] Super key enabled successfully" ${NORM}
}

#Aplicativos específicos a Xfce ou GNOME
APPSC () {
	if [[ ${INXI} = *Xfce* && (${INXI} = *Debian* || ${INXI} = *Ubuntu*) ]]; then #Tests if it is Debian Xfce base
		echo -e "\n${CIAN}[ ] Install APPs for Xfce" ${NORM}
			${SUDD} install ${PACDX} -y &&
		echo -e "${VERD}[*] Apps xfce instalados: \"${PACDX}\" " ${NORM}
	elif [[ ${INXI} = *GNOME* && (${INXI} = *Debian* || ${INXI} = *Ubuntu* || ${INXI} = *Pop*) ]]; then #Test if it's Debian GNOME
		echo -e "\n${CIAN}[ ] Install APPs for GNOME" ${NORM}
			${SUDD} install ${PACDG} -y &&
		echo -e "${VERD}[*] Apps GNOME instalados: \"${PACDX} \" " ${NORM}
	elif [[ ${INXI} = *GNOME* && ${INXI} = *Fedora* ]]; then
		echo -e "\n${CIAN}[ ] Install APPs for Fedora" ${NORM}
			${SUDF} install -y ${PACC} ${PACDG}&&
		echo -e "${VERD}[*] Apps GNOME installed: \"${PACC} \" " ${NORM}
	else
		echo -e "\n${VERM}[!] Unable to install add-on apps" ${NORM}
	fi
}

#Installation of common icons, themes, and customizations
PERCOM () {
	${CURL} ${GIT}temas.sh | bash
	${CURL} ${GIT}icones.sh | bash
	${CURL} ${GIT}persona.comum.sh | bash
}

# Start verification
ATUAS && APPSC && PERCOM
