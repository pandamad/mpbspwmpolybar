#!/usr/bin/env bash
# Desenvolvido pelo William Santos
# contato: thespation@gmail.com ou https://github.com/thespation

# Cores (tabela de cores: https://gist.github.com/avelino/3188137)
VERM="\033[1;31m"	#Output red
VERD="\033[0;32m"	#Output green
CIAN="\033[0;36m"	#Output cyan
NORM="\033[0m"		#Back default color

#Alias
GIT='https://raw.githubusercontent.com/thespation/dpux_bspwm/main/scripts/'
CURL='curl -s'

#Selection menu, available possibilities
MENU () {
	echo -e "\n${CIAN}[ ] Choose one of the available options\n" ${NORM}
	echo -e "${VERD}[1] Install and configure custom BSPWM" ${NORM}
	echo -e "[2] Install only the icons"
	echo -e "[3] Install only the themes" 
	echo -e "[4] Install all icons and themes" 
	echo -e "${VERM}[5] Exit\n" ${NORM}
	read -p "[?] Enter the desired option: "

	if [[ $REPLY == "1" ]]; then
		BSPWMPERS
	elif [[ $REPLY == "2" ]]; then
		ICONS
	elif [[ $REPLY == "3" ]]; then
		TEMAS
	elif [[ $REPLY == "4" ]]; then
		ICOTHE
	elif [[ $REPLY == "5" ]]; then
		exit 1
	else
		echo -e "\n${VERM}Unknown option\n" ${NORM} ; echo ;  MENU
	fi
}

#If option [1] base bspwm is chosen
BSPWMPERS () {
	${CURL} ${GIT}base.sh | bash
	MENU
}

ICONS () {
	${CURL} ${GIT}icones.sh | bash
	MENU
}

TEMAS () {
	${CURL} ${GIT}temas.sh | bash
	MENU
}

ICOTHE () {
	${CURL} ${GIT}icones.sh | bash && ${CURL} ${GIT}temas.sh | bash
	MENU	
}

# Iniciar Menu
clear; MENU
