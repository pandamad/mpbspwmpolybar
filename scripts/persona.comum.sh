#!/usr/bin/env bash
# Desenvolvido pelo William Santos
# contato: thespation@gmail.com ou https://github.com/thespation

# Cores (tabela de cores: https://gist.github.com/avelino/3188137)
VERM="\033[1;31m"	#Output red
VERD="\033[0;32m"	#Output green
CIAN="\033[0;36m"	#Output cyan
NORM="\033[0m"		#Back default color

# Ends, in case of runtime error
set -e

# Alias
DATA=`date +%F.%T`		PASTA="$HOME/.config/"	
REPO="https://github.com/thespation/dpux_bspwm"
GIT="git clone"			PTMP='/tmp/dpux_bspwm/home/'
COP="cp -rf"			SCOP="sudo cp -rf"
USRP='/usr/local/bin'

# Clone the repository with the customizations
BAIREP () {
	echo -e "\n${CIAN}[ ] Download repository${REPO}" ${NORM}
	cd /tmp && ${GIT} ${REPO} && chmod 755 dpux_bspwm/* -R
	echo -e "${VERD}[*] Repository in temp folder" ${NORM}
	COPI
}

# Copy custom files
COPI () {
	echo -e "\n${CIAN}[ ] Start copy process" ${NORM}
		${SCOP} /tmp/dpux_bspwm/fonts/* /usr/share/fonts
	echo -e "${VERD}[*] Fonts copied successfully" ${NORM}
	
	mv /${PTMP}Xresources.d ${PTMP}.Xresources.d && ${COP} ${PTMP}.Xresources.d $HOME
	${COP} ${PTMP}.Xresources $HOME
	${COP} ${PTMP}.gtkrc-2.0 $HOME
	${COP} ${PTMP}.xsettingsd $HOME
	${SCOP} ${PTMP}xubuntu/apps_as_root.sh ${USRP}
	${SCOP} ${PTMP}xubuntu/nmd ${USRP}
	${SCOP} ${PTMP}xubuntu/askpass_rofi.sh ${USRP}
	#echo -e "${VERD}[*] Customization files copied" ${NORM}
	
	PBSPWM && PDUNST && PGEANY && PGTK3 &&
	${COP} ${PTMP}config/* ~/.config
}

# Check if the bspwm folder exists, if you have it, make a backup before copying
PBSPWM () {
	if [[ -d "${PASTA}bspwm" ]]; then
		echo -e "\n${CIAN}[ ] Backing up the folder ${PASTA}bspwm" ${NORM}
		mv ${PASTA}bspwm ${PASTA}bspwm.bkp.${DATA}	
		echo -e "${VERD}[*] Backup performed successfully" ${NORM}
	fi
}

# Check if the dunst folder exists, if you have it, make a backup before copying
PDUNST () {
	if [[ -d "${PASTA}dunst" ]]; then
		echo -e "\n${CIAN}[ ] Backing up the folder ${PASTA}dunst" ${NORM}
		mv ${PASTA}dunst ${PASTA}dunst.bkp.${DATA}	
		echo -e "${VERD}[*] Backup performed successfully" ${NORM}
	fi
}

# Check if the geany folder exists, if you have it, make a backup before copying
PGEANY () {
	if [[ -d "${PASTA}geany" ]]; then
		echo -e "\n${CIAN}[ ] Backing up the folder ${PASTA}geany" ${NORM}
		mv ${PASTA}geany ${PASTA}geany.bkp.${DATA}	
		echo -e "${VERD}[*] Backup performed successfully" ${NORM}
	fi
}

# Check if the gtk-3.0 folder exists, if you have it, make a backup before copying
PGTK3 () {
	if [[ -d "${PASTA}gtk-3.0" ]]; then
		echo -e "\n${CIAN}[ ] Backing up the folder ${PASTA}gtk-3.0" ${NORM}
		mv ${PASTA}gtk-3.0 ${PASTA}gtk-3.0.bkp.${DATA} && mkdir -p ${PASTA}gtk-3.0
		${COP} ${PASTA}gtk-3.0.bkp.*/bookmarks ${PASTA}gtk-3.0/bookmarks 	#Copy custom shortcuts from thunar
		echo -e "${VERD}[*] Backup performed successfully" ${NORM}
	fi
}

# Start script
BAIREP
