#!/usr/bin/env bash
# Desenvolvido pelo William Santos
# contato: thespation@gmail.com ou https://github.com/thespation

# Cores (tabela de cores: https://gist.github.com/avelino/3188137)
VERM="\033[1;31m"	#Output red
VERD="\033[0;32m"	#Output green
CIAN="\033[0;36m"	#Output cyan
NORM="\033[0m"		#Back default color

set -e #Ends, in case of runtime error

# Distro ID Alias
VERU="22.04" 			#Current version of Ubuntu
GIT='https://raw.githubusercontent.com/thespation/dpux_bspwm/main/scripts/' #scripts folder
CURL='curl -s'			#Command for executing the script, without downloading

# Verificação da distro base
VERI () {
	INXI=`inxi -S`			#Needed to identify the DE (xfce or gnome)
	ID=`lsb_release -i`		#Identifies the distro version
	RELEASE=`lsb_release -r`	#Identifies the distro version
		if [[ $INXI = *Xfce* && $INXI = *$VERU* && $INXI = *Ubuntu* ]]; then #Test if it's Xubuntu
			echo -e "${VERD}[*] System supported, installation continue" ${NORM}
			echo -e "${VERD}[*] Xubuntu $VERU" ${NORM}
			${CURL} ${GIT}base.sh | bash && ${CURL} ${GIT}xubuntu.sh | bash
		elif [[ $ID = "Distributor ID:	Pop" && $RELEASE = "Release:	22.04" ]]; then #Test if it's PopOs
			${INXI} ; echo -e "${VERD}[*] System supported, installation continue" ${NORM} && sleep 3s
			${CURL} ${GIT}base.sh | bash
		elif [[ ${INXI} = *Xfce* && "Distributor ID:	Debian" && (${RELEASE} = "Release:	testing" || ${RELEASE} = "Release:	11") ]]; then #Test if it's Debian xfce
			${INXI} ; echo -e "${VERD}[*] System supported, installation continue" ${NORM} && sleep 3s
			${CURL} ${GIT}base.sh | bash
			${PAST}base.sh
		elif [[ ${INXI} = *GNOME* && ${INXI} = ${VERU} && ${INXI} = *Ubuntu* ]]; then #Test if it's Ubuntu
			${INXI} ; echo -e "${VERD}[*] System supported, installation continue" ${NORM} && sleep 3s
			${CURL} ${GIT}base.sh | bash
			#./scripts/ubuntu.sh
		elif [[ ${INXI} = *GNOME* && "Distributor ID:	Debian" && (${RELEASE} = "Release:	testing" || ${RELEASE} = "Release:	11") ]]; then #Test if it's Debian GNOME
			${INXI} ; echo -e "${VERD}[*] System supported, installation continue" ${NORM} && sleep 3s
			${CURL} ${GIT}base.sh | bash
			#./scripts/debiangnome.sh
		elif [[ ${INXI} = *GNOME* && ${INXI} = *Fedora* ]]; then
			${INXI} ; echo -e "${VERD}[*] System supported, installation continue" ${NORM} && sleep 3s
			${CURL} ${GIT}base.sh | bash
			#./scripts/fedora.sh
		else	
			clear && echo -e "\n${VERM}[!] Sistema, DE ou versão não suportada\n" ${NORM}
			echo "#----------This script was developed to run on the following distros:---------#"		
			echo "#-----------------Debian Bullseye or Bookworm (xfce e gnome)------------------#"
			echo "#------------------------------Fedora GNOME-----------------------------------#"
			echo "#------------------------------Pop!_Os 22.04----------------------------------#"
			echo "#--------------------------Ubuntu or Xubuntu 22.04----------------------------#"
			echo -e "#----If you are using one of this, it is necessary to have 'inxi' installed---#\n"		
			sleep 3
	fi
}
# start verification
clear
VERI
