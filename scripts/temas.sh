#!/usr/bin/env bash
# Desenvolvido pelo William Santos
# contato: thespation@gmail.com ou https://github.com/thespation

# Cores (tabela de cores: https://gist.github.com/avelino/3188137)
VERM="\033[1;31m"	#Make the output red
VERD="\033[0;32m"	#Make the output green
CIAN="\033[0;36m"	#Make the output cyan
NORM="\033[0m"		#Back to default color
## Alias
CC="sudo cp -rf"		##file copy command
GAT="/tmp/archcraft-themes/"	#Themes temporary folder
USRT="/usr/share/themes/"	#System themes folder
FP="/files/*"			#Alias for end of theme folder name

#Funtions
	# BAT - Test/download if the themes folder exists
	# INSTEMA - copy themes to system folder
BAT () {
	#Test if you have already downloaded the themes
	if [[ -d "${GAT}" ]]; then #Check if the temp folder for themes exists
		INSTEMA
	elif [[ ! -d "${GAT}" ]]; then #If not, download the themes
		echo -e "\n${CIAN}[ ] Downloading the themes for" ${GAT} ${NORM}
		cd /tmp && git clone https://github.com/archcraft-os/archcraft-themes &&
		chmod 755 archcraft-themes/* -R #Grant permission to the downloaded folder
		echo -e "${VERD}[*] themes in the folder" ${GAT} ${NORM}
		INSTEMA
	else
		FALH ${NORM} #Notifies in case of failure
	fi	
}	
	
INSTEMA () {
		echo -e "\n${CIAN}[ ] Copy themes for" $USRT ${NORM} # Copy themes to "/usr/share/themes"
		$CC $GAT'archcraft-gtk-theme-adapta'$FP $GAT'archcraft-gtk-theme-arc'$FP $GAT'archcraft-gtk-theme-blade'$FP \
		$GAT'archcraft-gtk-theme-cyberpunk'$FP $GAT'archcraft-gtk-theme-dracula'$FP $GAT'archcraft-gtk-theme-easy'$FP \
		$GAT'archcraft-gtk-theme-groot'$FP $GAT'archcraft-gtk-theme-gruvbox'$FP $GAT'archcraft-gtk-theme-hack'$FP \
		$GAT'archcraft-gtk-theme-juno'$FP $GAT'archcraft-gtk-theme-kripton'$FP $GAT'archcraft-gtk-theme-manhattan'$FP \
		$GAT'archcraft-gtk-theme-nordic'$FP $GAT'archcraft-gtk-theme-rick'$FP $GAT'archcraft-gtk-theme-slime'$FP \
		$GAT'archcraft-gtk-theme-spark'$FP $GAT'archcraft-gtk-theme-sweet'$FP $GAT'archcraft-gtk-theme-wave'$FP \
		$GAT'archcraft-gtk-theme-white'$FP $GAT'archcraft-gtk-theme-windows'$FP $USRT
				
	if [[ -d "$USRT/Spark" || "$USRT/Sweet-Dark" ]]; then #Checks if the folders are in the right place
		ls $USRT #Exibe a pasta de temas
		echo -e "${VERD}[*] Themes copied to the folder" $USRT ${NORM} #Notify success
	else
		echo -e "\n${VERM}[!] Unable to copy themes\n" ${NORM} #Notifies in case of failure
	fi
}
# Start the theme verification and installation process
BAT
